# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.

def guessing_game
  puts 'guess a number'
  secret_number = numbers
  user_guess = nil
  guesses = []
  until secret_number == user_guess
    puts user_guess = get_guess
    guesses << user_guess unless guesses.include?(user_guess)
    high_or_low(secret_number, user_guess)
  end
  won(secret_number, guesses)
end

def get_guess
  gets.chomp.to_i
end

def won(secret_number, guess_arr)
  puts "Correct! The number was #{secret_number}."
  puts "You needed #{guess_arr.size} guesses!"
  puts "===================================="
end

def high_or_low(secret_number, user_guess)
  if secret_number > user_guess
    puts 'too low'
    puts '____________________________________'
  end
  if secret_number < user_guess
    puts 'too high'
    puts '____________________________________'
  end
end

def numbers
  random_number = Random.new
  random_number.rand(1..100)
end

def file_shuffler
  puts 'Which file would you like to shuffle?'
  file_name = gets.chomp
  file_name_lines = File.readlines(file_name).shuffle
  File.open("#{file_name}-shuffled.txt", "w") do |new_file_lines|
    new_file_lines.puts file_name_lines.join
  end

end
